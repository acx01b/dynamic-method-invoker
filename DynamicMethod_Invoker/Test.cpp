// DynamicMethod_Invoker.cpp : main project file.

//#include "stdafx.h"
#include "DMI.h"

using namespace System;
using namespace System::Reflection;
using namespace System::Reflection::Emit;


value struct HMMVT {
	Int32 k;
	void Meth(Int32 u) {
		k++;
	}
};
ref struct HMM {
	HMMVT vt;
	HMMVT% getVT() {
		return vt;
	}
	int getU() {
		return 6;
	}
	void Meth() {
		getVT().Meth(getU());
	}

};

value struct TestConstructorVT {
	Int32 k;
	TestConstructorVT(Int32 ^n) {
		k = (Int32)n;
	}
	void Meth() {
		k++;
	}
};

value struct VT {
	int a, b;
};


ref struct TestConstructor {
	Object ^o;
	Int32 m;
	VT vt;
	String ^Lala;
	property String ^ lala {
		String ^get() { return Lala; }
		void set(String ^s) { Lala = s; }
	}
	TestConstructor(String ^s) {
		o = s;
	}
};


int p;

ref struct A {
	Int32 N;
	VT fvt;
	VT *fvtptr;
	Object ^obj;
	Object^ Meth1(String ^s, String^ s2) {
		N++;
		return s + s2;
	}
	static void Meth2() {
		p = 1;
	}
	virtual void Meth3(String ^s, String^ s2, VT vt) {
		Console::WriteLine(s + s2);
	}

	String^ Meth4(VT* vt) {
		return IntPtr(vt).ToString() + vt->a.ToString() + vt->b.ToString();
	}
	void Meth5(String ^% s) {
		s = "apres";
	}
	void Meth6(Int32% n) {
		n++;
	}
	void Meth7(VT% vt) {
		vt.b++;
	}

	
	VT Meth8() {
 		VT vt;
		vt.a = 4;
		vt.b = 5;
		return vt;
	}

	VT* Meth9() {
		VT * vt = new VT;
		vt->a = 4;
		vt->b = 5;
		return vt;
	}
};

ref struct B : A {
	String ^someStr;
	virtual void Meth3(String ^s, String^ s2, VT  vt) override {
		someStr = s + s2 + vt.a.ToString() + vt.b.ToString();
	}


	void Test(A ^a) {
		pin_ptr<Object^> tmp = &a->obj;
		void * tmp2 = tmp;
	}

};

void Assert(Boolean b) {
	if (!b) {
		throw gcnew Exception("b == false");
	}
}


int main(array<System::String ^> ^args)
{
	A ^a = gcnew A();
	B ^b = gcnew B();
	
	
	/// call : non virtual method
    /// param : reference type 
	/// return : reference type
	DMIDelegate^ del1 = DynamicInvoker::Create(A::typeid->GetMethod("Meth1"));
	array<Object ^>^ arr1 = { a, "hello", " world" };
	a->N = 8;
	Object ^res1 = del1->Invoke(arr1);
	Assert(((A^)a)->N == 9);
	Assert((String^)res1 == "hello world");

	/// call : static method
	/// return : void
	DMIDelegate^ del2 = DynamicInvoker::Create(A::typeid->GetMethod("Meth2"));
	array<Object ^>^ arr2 = {};
	p = 0;
	Object ^res2 = del2->Invoke(arr2);
	Assert(p == 1);
	Assert(res2 == nullptr);

	/// call : virtual method
    /// param : value type
	VT vt3; vt3.a = 8; vt3.b = 9;
	DMIDelegate^ del3 = DynamicInvoker::Create(A::typeid->GetMethod("Meth3"));
	array<Object ^>^ arr3 = { b, "hello", " world", vt3 };
	Object ^res3 = del3->Invoke(arr3);
	Assert(b->someStr == "hello world89");
	Assert(res3 == nullptr);

	/// param : pointer
	VT vt4; vt4.a = 8; vt4.b = 9;
	DMIDelegate^ del4 = DynamicInvoker::Create(A::typeid->GetMethod("Meth4"));
	array<Object ^>^ arr4 = { b, (IntPtr)&vt4 };
	Object ^res4 = del4->Invoke(arr4);
	Assert((String^)res4 == IntPtr(&vt4).ToString() + vt4.a.ToString() + vt4.b.ToString());


	/// param : tracking reference on reference type
	String ^s5 = "avant";
	TrackingReference_Ldloca ^tr5 = gcnew TrackingReference_Ldloca(&s5);
	array<Object ^>^ arr5 = { a, tr5 };
	DMIDelegate^ del5 = DynamicInvoker::Create(A::typeid->GetMethod("Meth5"));
	Object ^res5 = del5->Invoke(arr5);
	Assert(s5 == "apres");

	array<String^> ^strarr = { "avant", "avant" };
	TrackingReference_Ldelema<String^> ^tr5bis = gcnew TrackingReference_Ldelema<String^>(strarr, 1);
	array<Object ^>^ arr5bis = { a, tr5bis };
	del5->Invoke(arr5bis);
	Assert(s5 == "apres");

	
	/// param : tracking reference on ValueType
	VT vt7; vt7.b = 88;
	TrackingReference_Ldloca ^tr7 = gcnew TrackingReference_Ldloca(&vt7);
	array<Object ^>^ arr7 = { a, tr7 };
	DMIDelegate^ del7 = DynamicInvoker::Create(A::typeid->GetMethod("Meth7"));
	Object ^res7 = del7->Invoke(arr7);
	Assert(vt7.b == 89);

	array<VT> ^vtarr = gcnew array<VT>(2);
	vtarr[1].b = 7;
	TrackingReference_Ldelema<VT> ^tr7bis = gcnew TrackingReference_Ldelema<VT>(vtarr,1);
	array<Object ^>^ arr7bis = { a, tr7bis };
	del7->Invoke(arr7bis);
	Assert(vtarr[1].b == 8);

	
	
	/// return : ValueType
	DMIDelegate^ del8 = DynamicInvoker::Create(A::typeid->GetMethod("Meth8"));
	array<Object ^>^ arr8 = { a };
	Object ^res8 = del8->Invoke(arr8);
	Assert(((VT^)res8)->a == 4);
	Assert(((VT^)res8)->b == 5);

	/// return : pointer
	DMIDelegate^ del9 = DynamicInvoker::Create(A::typeid->GetMethod("Meth9"));
	array<Object ^>^ arr9 = { a };
	Object ^res9 = del9->Invoke(arr9);
	VT * vtptr = (VT*)(void*)(IntPtr)res9;
	Assert(vtptr->a == 4);
	Assert(vtptr->b == 5);

	/// constructor : reference type 
	DMIDelegate^ del10 = DynamicInvoker::Create(TestConstructor::typeid->GetConstructors()[0]);
	array<Object ^>^ arr10 = { "hello world"};
	Object ^res10 = del10->Invoke(arr10);
	Assert( (String^)((TestConstructor^)res10)->o == "hello world");

	/// constructor : value type 
	DMIDelegate^ del11 = DynamicInvoker::Create(TestConstructorVT::typeid->GetConstructors()[0]);
	array<Object ^>^ arr11 = {(Int32)13};
	Object ^res11 = del11->Invoke(arr11);
	Assert(((TestConstructorVT^)res11)->k == 13);


	/// field : reference type
	DMIGetterSetterAddress ^fieldGetterSetter12 = DynamicInvoker::Create(B::typeid->GetField("someStr"));
	{
		// getter
		b->someStr = "youhou";
		array<Object ^>^ arr12 = { b };
		Object ^res12 = fieldGetterSetter12->getter->Invoke(arr12);
		Assert((String^)res12 == "youhou");
		// setter
		array<Object ^>^ arr12s = { b, "hmmm" };
		Object ^res12s = fieldGetterSetter12->setter->Invoke(arr12s);
		Assert(b->someStr == "hmmm");

		/// address
		DMIDelegate^ del7bis = DynamicInvoker::Create(A::typeid->GetMethod("Meth5"));
		TrackingReference_Ldflda ^tr7bis = gcnew TrackingReference_Ldflda(b, fieldGetterSetter12);
		
		array<Object ^>^ arr7bis = { b, tr7bis };
		b->someStr = "lkjlkj";
		del7bis->Invoke(arr7bis);
		Assert(b->someStr == "apres");
	}

	

	/// field : value type
	DMIGetterSetterAddress ^fieldGetterSetter13 = DynamicInvoker::Create(A::typeid->GetField("fvt"));
	{
		// getter
		a->fvt.a = 14;
		a->fvt.b = 15;
		array<Object ^>^ arr13 = { a };
		Object ^res13 = fieldGetterSetter13->getter->Invoke(arr13);
		Assert(((VT^)res13)->a == 14);
		Assert(((VT^)res13)->b == 15);
		// setter
		VT tmp; tmp.a = 77; tmp.b = 78;
		array<Object ^>^ arr13s = { a, tmp };
		Object ^res12s = fieldGetterSetter13->setter->Invoke(arr13s);
		Assert(a->fvt.a == 77);
		Assert(a->fvt.b == 78);

		/// address
		DMIDelegate^ del7bis = DynamicInvoker::Create(A::typeid->GetMethod("Meth7"));
		TrackingReference_Ldflda ^tr7bis = gcnew TrackingReference_Ldflda(a, fieldGetterSetter13);
		array<Object ^>^ arr7bis = { a, tr7bis };
		a->fvt.b = 7;
		del7bis->Invoke(arr7bis);
		Assert(a->fvt.b == 8);

	}

	/// field : pointer
	DMIGetterSetter ^fieldGetterSetter14 = DynamicInvoker::Create(B::typeid->GetField("fvtptr"));
	{
		// getter
		VT tmp;
		a->fvtptr = &tmp;
		array<Object ^>^ arr14 = { a };
		Object ^res14 = fieldGetterSetter14->getter->Invoke(arr14);
		Assert((IntPtr)res14 == IntPtr(&tmp));
		// setter
		VT tmp2;
		array<Object ^>^ arr13s = { a, IntPtr(&tmp2) };
		Object ^res12s = fieldGetterSetter14->setter->Invoke(arr13s);
		Assert(a->fvtptr == &tmp2);
	}
	
	/// property
	{
		TestConstructor ^tmp = gcnew TestConstructor("lkjlkj");
		DMIGetterSetter ^propGetterSetter13 = DynamicInvoker::Create(TestConstructor::typeid->GetProperty("lala"));
		tmp->lala = "yyy";
		array<Object ^>^ arr12 = { tmp };
		Object ^res = propGetterSetter13->getter->Invoke(arr12);
		Assert((String^)res == "yyy");
		array<Object ^>^ arr12s = { tmp, "zzz" };
		Object ^ress = propGetterSetter13->setter->Invoke(arr12s);
		Assert(tmp->lala == "zzz");
	}

	/// Method of a value type
	{
		TestConstructorVT tmp;
		tmp.k = 87;
		TrackingReference_Ldloca ^tr = gcnew TrackingReference_Ldloca(&tmp);
		DMIDelegate ^del13 = DynamicInvoker::Create(TestConstructorVT::typeid->GetMethod("Meth"));
		array<Object ^>^ arr13 = { tr, (Int32)44 };
		del13->Invoke(arr13);
		Assert(tmp.k == 88);
	}

	/// field of a value type
	{
		TestConstructorVT tmp;
		TrackingReference_Ldloca ^tr = gcnew TrackingReference_Ldloca(&tmp);
		DMIGetterSetter ^fieldGetterSetter13 = DynamicInvoker::Create(TestConstructorVT::typeid->GetField("k"));
		array<Object ^>^ arr12s = { tr, (Int32)44 };
		fieldGetterSetter13->setter->Invoke(arr12s);
		Assert(tmp.k == 44);
	}
	
    return 0;
}
