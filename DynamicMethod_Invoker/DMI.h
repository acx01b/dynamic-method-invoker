
delegate System::Object ^ DMIDelegate(array<System::Object^>^);
delegate System::IntPtr DelegateFieldAddres(System::Object^);

ref struct DMIGetterSetter;
ref struct DMIGetterSetterAddress;
ref struct TrackingReference_Ldloca;
generic <typename U> ref struct TrackingReference_Ldelema;
ref struct TrackingReference_Ldflda;

ref struct DynamicInvoker {
	static DMIDelegate^ Create(System::Reflection::MethodInfo ^m);
	static DMIDelegate^ Create(System::Reflection::ConstructorInfo ^f);
	static DMIGetterSetterAddress^ Create(System::Reflection::FieldInfo ^f);
	static DMIGetterSetter^ Create(System::Reflection::PropertyInfo ^f);
};

ref struct TrackingReference abstract {
	virtual System::Object^% getRT() = 0;
	virtual System::Int32% getVT() = 0;

	static TrackingReference_Ldloca^ Create(void * ptr) { return nullptr; }
	template <typename T> static TrackingReference_Ldloca^ Create(T& r) { return nullptr; }
	generic <typename U> static TrackingReference_Ldelema<U>^ Create(System::Object ^tab, int index) { return nullptr; }
	generic <typename U> static TrackingReference_Ldelema<U>^ Create(array<U>^ tab, int index) { return nullptr; }
	static TrackingReference_Ldflda^ Create(System::Object^ o, DelegateFieldAddres ^del) { return nullptr; }
	static TrackingReference_Ldflda^ Create(System::Object^ o, DMIGetterSetterAddress ^field) { return nullptr; }
};

ref struct TrackingReference_Ldloca : TrackingReference {
protected:
	void * ptr;
public:
	TrackingReference_Ldloca(void * ptr) : ptr(ptr) {}
	template <typename T> TrackingReference_Ldloca(T& r) : ptr(&r) {}
	virtual System::Object^% getRT() override {
		return *(System::Object^*)ptr;
	}
	virtual System::Int32% getVT() override {
		return *(System::Int32*)ptr;
	}
};

generic <typename U> ref struct TrackingReference_Ldelema : TrackingReference {
protected:
	System::Object ^tab;
	int index;
public:
	TrackingReference_Ldelema(System::Object ^tab, int index) : tab(tab), index(index) {}
	virtual System::Object^% getRT() override {
		return *(interior_ptr<System::Object^>)&((array<U>^)tab)[index];
	}
	virtual System::Int32% getVT() override {
		return *(interior_ptr<System::Int32>)&((array<U>^)tab)[index];
	}
};

ref struct DMIGetterSetter {
	DMIDelegate^ getter;
	DMIDelegate^ setter;
};

ref struct DMIGetterSetterAddress : DMIGetterSetter {
	DelegateFieldAddres^ address;
};

ref struct TrackingReference_Ldflda : TrackingReference {
protected:
	DelegateFieldAddres ^del; // returns field address as IntPtr (object should be pinned prior to call)
	System::Object ^o;
public:
	TrackingReference_Ldflda(System::Object^ o, DelegateFieldAddres ^del) : o(o), del(del) {}
	TrackingReference_Ldflda(System::Object^ o, DMIGetterSetterAddress ^field) : o(o), del(field->address) {}
	virtual System::Object^% getRT() override {
		pin_ptr<System::Object^> tmp = &o;
		System::IntPtr adr = del->Invoke(o);
		return *(interior_ptr<System::Object^>)(void*)adr;
	}
	virtual System::Int32% getVT() override {
		pin_ptr<System::Object^> tmp = &o;
		System::IntPtr adr = del->Invoke(o);
		return *(interior_ptr<System::Int32>)(void*)adr;
	}
};





