#include "DMI.h"
#include <gcroot.h>
using namespace System;
using namespace System::Reflection;
using namespace System::Reflection::Emit;

bool IsPseudoReferenceType(Type ^t) {
	if (t->IsClass || t->IsArray || t->IsInterface) {
		return true;
	}
	return false;
}

bool IsPseudoValueType(Type ^t) {
	if (t->IsValueType || t->IsPointer) {
		return true;
	}
	return false;
}

void EmitParams(array<Type^> ^paramTypes, ILGenerator ^ gen, bool isMethod) {
	int L = paramTypes->Length;
	
	static gcroot<MethodInfo^> TRgetVT;

	for (int k = 0; k < L; k++) {
		gen->Emit(OpCodes::Ldarg_0);
		gen->Emit(OpCodes::Ldc_I4, k);
		gen->Emit(OpCodes::Ldelem, Object::typeid);
		
		Type ^curParmT = paramTypes[k];
		if (curParmT->IsValueType) {
			if (isMethod && k == 0) {
				if ((MethodInfo^)TRgetVT == nullptr) {
					TRgetVT = TrackingReference::typeid->GetMethod("getVT");
				}
				gen->Emit(OpCodes::Callvirt, TRgetVT);
			} else {
				gen->Emit(OpCodes::Unbox, curParmT);
				gen->Emit(OpCodes::Ldobj, curParmT);
			}
		} else if (curParmT->IsPointer) {
			gen->Emit(OpCodes::Unbox, IntPtr::typeid);
			gen->Emit(OpCodes::Ldobj, IntPtr::typeid);
		} else if (curParmT->IsByRef) {
			MethodInfo ^mget;
			Type ^elemT = curParmT->GetElementType();
			if (IsPseudoReferenceType(elemT)) {
				static gcroot<MethodInfo^> TRgetRT;
				if ((MethodInfo^)TRgetRT == nullptr) {
					TRgetRT = TrackingReference::typeid->GetMethod("getRT");
				}
				mget = TRgetRT;
			} else if (IsPseudoValueType(elemT)) {
				
				if ((MethodInfo^)TRgetVT == nullptr) {
					TRgetVT = TrackingReference::typeid->GetMethod("getVT");
				}
				mget = TRgetVT;
			}
			gen->Emit(OpCodes::Callvirt, mget);
		}
	}
}

array<Type^> ^GetTypes(array<ParameterInfo^>^ params, Type ^type, bool isStatic) {

	array<Type^> ^paramTypes;
	
	int ind = 0;
	if (!isStatic) {
		paramTypes = gcnew array<Type^>(params->Length + 1);
		paramTypes[0] = type;
		ind++;
	} else {
		paramTypes = gcnew array<Type^>(params->Length);
	}
	for (int k = 0; k < params->Length; k++) {
		paramTypes[ind] = params[k]->ParameterType;
		ind++;
	}
	return paramTypes;
}

DMIDelegate^ DynamicInvoker::Create(MethodInfo ^m) {
	array<Type^> ^ dyncMethParms = { array<Object^>::typeid };
	DynamicMethod ^ dynMeth = gcnew DynamicMethod("Stub__" + m->DeclaringType->Name + "__" + m->Name,
		Object::typeid, dyncMethParms,
		DMIDelegate::typeid->Module);

	ILGenerator ^ gen = dynMeth->GetILGenerator();

	EmitParams(GetTypes(m->GetParameters(), m->DeclaringType, m->IsStatic), gen, !m->IsStatic);


	if (m->IsVirtual) {
		gen->Emit(OpCodes::Callvirt, m);
	}
	else {
		/// TODO va_Args (EmitCall au lieu de Emit)
		gen->Emit(OpCodes::Call, m);
	}

	if (m->ReturnType == Void::typeid) {
		gen->Emit(OpCodes::Ldnull);
	} else {
		if (m->ReturnType->IsValueType) {
			gen->Emit(OpCodes::Box, m->ReturnType);
		}
		else if (m->ReturnType->IsPointer) {
			gen->Emit(OpCodes::Box, IntPtr::typeid);
		}
		else if (!IsPseudoReferenceType(m->ReturnType)) {
			throw gcnew Exception("unsupported return type : " + m->ReturnType->ToString());
		}
		
	}
	gen->Emit(OpCodes::Ret);
	DMIDelegate^ res = (DMIDelegate^)dynMeth->CreateDelegate(DMIDelegate::typeid);
	return res;
}




DMIDelegate^ DynamicInvoker::Create(ConstructorInfo ^c) {
	array<Type^> ^ dyncMethParms = { array<Object^>::typeid };
	DynamicMethod ^ dynMeth = gcnew DynamicMethod("Stub__" + c->DeclaringType->Name + "__ctor",
		Object::typeid, dyncMethParms,
		DMIDelegate::typeid->Module);

	ILGenerator ^ gen = dynMeth->GetILGenerator();

	EmitParams(GetTypes(c->GetParameters(), nullptr, true), gen, false);


		/// TODO va_Args (EmitCall au lieu de Emit)
	gen->Emit(OpCodes::Newobj, c);
	if (c->DeclaringType->IsValueType) {
		gen->Emit(OpCodes::Box, c->DeclaringType);
	}
	else if (!IsPseudoReferenceType(c->DeclaringType)) {
		throw gcnew Exception("unsupported constructor type : " + c->DeclaringType->ToString());
	}
	gen->Emit(OpCodes::Ret);
	DMIDelegate^ res = (DMIDelegate^)dynMeth->CreateDelegate(DMIDelegate::typeid);
	return res;
}

DMIGetterSetterAddress^ DynamicInvoker::Create(System::Reflection::FieldInfo ^f) {

	array<Type^> ^ dyncMethParms = { array<Object^>::typeid };

	/// getter
	DynamicMethod ^ dynMeth = gcnew DynamicMethod("Stub__" + f->DeclaringType->Name + "__" + f->Name + "__getter",
		Object::typeid, dyncMethParms,
		DMIDelegate::typeid->Module);

	ILGenerator ^ gen = dynMeth->GetILGenerator();

	gen->Emit(OpCodes::Ldarg_0);
	gen->Emit(OpCodes::Ldc_I4_0);
	gen->Emit(OpCodes::Ldelem, Object::typeid);

	gen->Emit(OpCodes::Ldfld, f);
	if (f->FieldType->IsValueType) {
		gen->Emit(OpCodes::Box, f->FieldType);
	} else if (f->FieldType->IsPointer) {
		gen->Emit(OpCodes::Box, IntPtr::typeid);
	} else if (!IsPseudoReferenceType(f->FieldType)) {
		throw gcnew Exception("unsupported constructor type : " + f->FieldType->ToString());
	}
	gen->Emit(OpCodes::Ret);
	DMIDelegate ^getter = (DMIDelegate^)dynMeth->CreateDelegate(DMIDelegate::typeid);

	/// setter

	dynMeth = gcnew DynamicMethod("Stub__" + f->DeclaringType->Name + "__" + f->Name + "__setter",
		Object::typeid, dyncMethParms,
		DMIDelegate::typeid->Module);

	gen = dynMeth->GetILGenerator();

	gen->Emit(OpCodes::Ldarg_0);
	gen->Emit(OpCodes::Ldc_I4_0);
	gen->Emit(OpCodes::Ldelem, Object::typeid);
	if (f->DeclaringType->IsValueType) {
		static gcroot<MethodInfo^> TRgetVT;
		if ((MethodInfo^)TRgetVT == nullptr) {
			TRgetVT = TrackingReference::typeid->GetMethod("getVT");
		}
		gen->Emit(OpCodes::Callvirt, TRgetVT);
	}
	

	gen->Emit(OpCodes::Ldarg_0);
	gen->Emit(OpCodes::Ldc_I4_1);
	gen->Emit(OpCodes::Ldelem, Object::typeid);

	
	if (f->FieldType->IsValueType) {
		gen->Emit(OpCodes::Unbox, f->FieldType);
		gen->Emit(OpCodes::Ldobj, f->FieldType);
	}
	else if (f->FieldType->IsPointer) {
		gen->Emit(OpCodes::Unbox, IntPtr::typeid);
		gen->Emit(OpCodes::Ldobj, IntPtr::typeid);
	}
	else if (!IsPseudoReferenceType(f->FieldType)) {
		throw gcnew Exception("unsupported constructor type : " + f->FieldType->ToString());
	}
	gen->Emit(OpCodes::Stfld, f);
	gen->Emit(OpCodes::Ldnull);
	gen->Emit(OpCodes::Ret);
	DMIDelegate ^setter = (DMIDelegate^)dynMeth->CreateDelegate(DMIDelegate::typeid);

	//// field address
	static gcroot<array<Type^>^> oneObjectParam;
	if ((array<Type^>^)oneObjectParam == nullptr) {
		array<Type^> ^tmp = { Object::typeid };
		oneObjectParam = tmp;
	}
	
	dynMeth = gcnew DynamicMethod("Stub__" + f->DeclaringType->Name + "__" + f->Name + "__TR",
		IntPtr::typeid, oneObjectParam,
		DMIDelegate::typeid->Module,true);	

	gen = dynMeth->GetILGenerator();
	if (!f->IsStatic) {
		gen->Emit(OpCodes::Ldarg_0);
		gen->Emit(OpCodes::Ldflda, f);
	} else {
		gen->Emit(OpCodes::Ldsflda, f);
	}
	gen->Emit(OpCodes::Ret);
	DelegateFieldAddres ^address = (DelegateFieldAddres^)dynMeth->CreateDelegate(DelegateFieldAddres::typeid);

	DMIGetterSetterAddress ^res = gcnew DMIGetterSetterAddress();
	res->address = address;
	res->getter = getter;
	res->setter = setter;
	return res;
}

DMIGetterSetter^ DynamicInvoker::Create(System::Reflection::PropertyInfo ^p) {
	DMIDelegate ^getter = Create(p->GetMethod);
	DMIDelegate ^setter = Create(p->SetMethod);
	DMIGetterSetter ^res = gcnew DMIGetterSetter();
	res->getter = getter;
	res->setter = setter;
	return res;
}
